package secondweekpractice;
import java.util.Scanner;
/*
Даны три целых числа a, b, c.
Проверить есть ли среди них прямо противоположные.
( 5 и -5 прямо противоположные числа).
0 и 0 не считать прямо противоположными.
Входные данные
-1 1 0
Выходные данные
true
Входные данные
-2 1 0
Выходные данные
false
 */

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        if (Math.abs(a) == Math.abs(b) ||
                Math.abs(a) == Math.abs(c) ||
                Math.abs(b) == Math.abs(c)){
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}

