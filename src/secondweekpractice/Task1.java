package secondweekpractice;
/*
Дано число n Нужно проверить четное ли оно
 */

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        String str;
        str = (n % 2 ==0) ? "число четное" : "число не четное";
/*
        if (n % 2 == 0) {
            str = "число четное";
        } else {
            str = "число не четное";
        }

 */
        System.out.print(str);

    }
}
