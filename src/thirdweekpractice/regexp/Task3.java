package thirdweekpractice.regexp;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        String date = scanner.nextLine();
        String phone = scanner.nextLine();
        String email = scanner.nextLine();

        name.matches("[A-Z][a-z]{1,19}");
        date.matches("[0-3][0-9]\\.\\d{2}\\.\\d{4}");
        phone.matches("\\+[0-9]{11}");
        email.matches("[A-Za-z0-9]\\-\\_\\*\\.]+@[a-z0-9]+\\.(com|ru)");
    }
}
