package thirdweekpractice.regexp;
/*
Проверить, является ли введеная строка корректным hex номером цвета.
Корректная строка состоит из 7 символов, первый символ #, далее цифры или буквы от A до F (заклавные или прописные)
Если строка является корректный hex номером цвета, то вывести tru, иначе false
#00AA12
true

00FFFF
false

#00FA
false
 */

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        String inputColor = new Scanner(System.in).nextLine();
        System.out.println("Our input data: " + inputColor);
        System.out.println(inputColor.matches("#[0-9A-Fa-f]{6}"));
    }
}
